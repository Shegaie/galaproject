import React, {useEffect, useState} from 'react';

import makeStyles from "@material-ui/core/styles/makeStyles";
import {Paper} from "@material-ui/core";
import Background from "./Background";
import oscar_back from "./assets/oscar2.jpg";

const useStyles = makeStyles(theme => ({
    main: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        height: '50%'
    }
}));

export default function Login(props) {
    const classes = useStyles();

    useEffect(() => {
    }, []);

    return (
        <div className={classes.main}>
            <Background src={oscar_back}/>
            <Paper elevation={40} className={classes.content}>

            </Paper>
        </div>
    )
}
