import React, {useState} from 'react';
import {HashRouter as BrowserRouter, Route, Switch} from 'react-router-dom';
import Login from './Login'
import Oscar from "./Oscar";
import Defi from "./Defi";
import './index.css'

function App() {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/">
                        <Login/>
                    </Route>
                    <Route exact path={'/oscar'}>
                        <Oscar/>
                    </Route>
                    <Route exact path={'/defi'}>
                        <Defi/>
                    </Route>
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
