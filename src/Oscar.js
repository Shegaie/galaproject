import React, {useEffect} from 'react';
import oscar_photo from './assets/oscar_photo.png'

import makeStyles from "@material-ui/core/styles/makeStyles";
import Background from "./Background";
import {Button} from "@material-ui/core";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    main: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    photoContainer: {
        width: '100%',
        height: '100%'
    }
}));

export default function Oscar(props) {
    const classes = useStyles();
    const history = useHistory();

    useEffect(() => {
    }, []);

    return (
        <div className={classes.main}>
            <Background src={oscar_photo}/>
            <Button onClick={() => {history.push("/defi")}} style={{
                fontSize: '16px',
                backgroundColor: '#ffba0e',
                color: 'white'
            }}>Bien ouej ! <br/>click ici pour la suite</Button>
        </div>
    )
}
