import React, {useEffect, useState} from 'react';

import makeStyles from "@material-ui/core/styles/makeStyles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";
import Toast from 'light-toast';

const useStyles = makeStyles(theme => ({
    main: {
        width: '100%',
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {
        width: '500px',
        height: '30%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    label: {
        width: '90%',
        height: '40px',
        display: 'flex',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        fontWeight: 'bold'
    },
    textField: {
        height: '40px',
    }
}));

export default function Login(props) {
    const classes = useStyles();
    const [password, setPassword] = useState("");
    const history = useHistory();

    useEffect(() => {
    }, []);

    const validatePassword = () => {
        if (password === "oscar")
            history.push('/oscar');
        else
            Toast.fail("Lame, you suck! Try again", 1000);
    };

    return (
        <div className={classes.main}>
            <form onSubmit={validatePassword} className={classes.content}>
                <div className={classes.label}>
                    <span style={{fontSize: '20px'}}>🧐</span><span>Entrer le mot de passe</span><span
                    style={{fontSize: '20px'}}>🧐</span>
                </div>
                <div style={{width: '100%', height: '20%'}}>
                    <TextField value={password} onChange={(evt) => setPassword(evt.target.value)} InputProps={{classes: {input: classes.textField}}} fullWidth placeholder="Mot de passe"/>
                </div>
                <div style={{
                    width: '100%',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                    <Button
                        style={{
                            width: '80%',
                            fontSize: '16px',
                            backgroundColor: 'black',
                            color: 'white'
                        }}
                        onClick={validatePassword}
                    >
                        Valider
                    </Button>
                </div>
            </form>
        </div>
    )
}
